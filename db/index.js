const mongoose = require('mongoose');

const { db } = require('../config');

mongoose.connect(db.uri, (err) => {
	if (err) return console.log('Could not connect to DB');
});

const nftSchema = new mongoose.Schema({
	_id: Number,
	owner: String,
	ipfsHash: String,
	meta: Object,
});

const nftModel = mongoose.model('Nft', nftSchema);

module.exports = {
	schema: nftSchema,
	model: nftModel,
};
