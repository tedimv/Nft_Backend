const fs = require('fs');

const rootDir = __dirname;
const walletDir = `${rootDir}/walletConfig`;
const policyDir = `${walletDir}/policy`;

const wallet = {
	escapeAddr:
		'addr1q83gtmzs0ycxrum36rju2lhng90zhknhcktfeufwtlgxuk5ua4073u4xeepk772s4x8xsgm4f5vpjlm0puuccn357y7qwuf25a',
	paymentAddr: {
		path: `${walletDir}/payment.addr`,
		value: readFile(`${walletDir}/payment.addr`),
	},
	paymentSkey: {
		path: `${walletDir}/payment.skey`,
		value: readFile(`${walletDir}/payment.skey`, true),
	},
	paymentVkey: {
		path: `${walletDir}/payment.vkey`,
		value: readFile(`${walletDir}/payment.vkey`, true),
	},
	protocol: {
		path: `${walletDir}/protocol.json`,
		value: readFile(`${walletDir}/protocol.json`, true),
	},
	policyScript: {
		path: `${policyDir}/policy.script`,
		value: readFile(`${policyDir}/policy.script`, true),
	},
	policySkey: {
		path: `${policyDir}/policy.skey`,
		value: readFile(`${policyDir}/policy.skey`, true),
	},
	policyVkey: {
		path: `${policyDir}/policy.vkey`,
		value: readFile(`${policyDir}/policy.vkey`, true),
	},
	policyID: {
		path: `${policyDir}/policyID`,
		value: readFile(`${policyDir}/policyID`),
	},
};

const pinata = {
	baseUrl: 'https://api.pinata.cloud', // +-psa
	apiKey: '36aa849305a6edb71de6',
	apiSecret: '9ce6ed13f64752126768d482048abebde3abc607759a3a720f74d528e18152db',
	jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiIwZDZmZmI5NC1mMDBlLTQwYjctYjM2Yi0xMWM5MjgzMjAyYmIiLCJlbWFpbCI6InRlb2RvcmRpbWl0cm92MTk5MUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicGluX3BvbGljeSI6eyJyZWdpb25zIjpbeyJpZCI6Ik5ZQzEiLCJkZXNpcmVkUmVwbGljYXRpb25Db3VudCI6MX1dLCJ2ZXJzaW9uIjoxfSwibWZhX2VuYWJsZWQiOmZhbHNlfSwiYXV0aGVudGljYXRpb25UeXBlIjoic2NvcGVkS2V5Iiwic2NvcGVkS2V5S2V5IjoiMzZhYTg0OTMwNWE2ZWRiNzFkZTYiLCJzY29wZWRLZXlTZWNyZXQiOiI5Y2U2ZWQxM2Y2NDc1MjEyNjc2OGQ0ODIwNDhhYmViZGUzYWJjNjA3NzU5YTNhNzIwZjc0ZDUyOGUxODE1MmRiIiwiaWF0IjoxNjMxOTAwNzg4fQ.jzLx_r2ShhJ-UjmejoTmlGi1c236R1DoS0tlZS5vsdE',
};

const cardanoCli = {
	socketPath: process.env.CARDANO_NODE_SOCKET_PATH,
	shelleyGenesisPath: '~/cnode/config/mainnet-shelley-genesis.json',
	address: readFile(`${walletDir}/payment.addr`),
};

const blockfrost = {
	apiKey: 'Zqus3GyK089wmbuFM4V3SEClnI6L7fem',
};

const nft = {
	name: 'DevNFT',
	total: 2,
};

const db = {
	uri: 'mongodb+srv://mapmota:WatpmbUYRZJnDazu@cluster0.rrhyr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
};

function readFile(path = '', toJson = false) {
	const content = fs.readFileSync(path, { encoding: 'ascii' });
	return toJson ? JSON.parse(content) : content;
}

module.exports = {
	rootDir,
	wallet,
	pinata,
	cardanoCli,
	blockfrost,
	nft,
	db,
};
