**Execution order**

1. Run [ _**pinata/deleteOldImages**_] - clear **pinata.cloud** storage 
2. Run [ _**pinata/upload_with_meta**_ ] - upload to **pinata.cloud** - result files should be added to DB
3. Run [ _**mongodb/hydrate**_ ] - using files from step (1) fill DB so it can be updated later