const fs = require('fs');
const CardanocliJs = require('cardanocli-js');
const { BlockFrostAPI } = require('@blockfrost/blockfrost-js');

const { blockfrost, cardanoCli, wallet } = require('../../config');
const { getTxOut } = require('./helpers');

// Required
const cardanoCliJs = new CardanocliJs({
	socketPath: cardanoCli.socketPath,
	shelleyGenesisPath: cardanoCli.shelleyGenesisPath,
	dir: __dirname.replace('/server', ''),
});
const bfAPI = new BlockFrostAPI({
	isTestnet: false,
	projectId: blockfrost.apiKey,
});
const ignoreList = new Set();
// Required

async function handleRefund(txHash, txId) {
	if (ignoreList.has(txHash)) return;

	try {
		const { inputs, outputs } = await bfAPI.txsUtxos(txHash);
		const customerAddress = inputs[0].address;

		const wrongOutput = outputs.find(
			(output) => output.address === wallet.paymentAddr.value
		);
		const { unit, quantity } = wrongOutput.amount[0];

		// Confirm the currency is correct/prevent random bullshit
		if (unit !== 'lovelace') {
			fs.writeFileSync(
				`${__dirname}/errors/${txHash}`,
				`${txHash},${unit},${quantity}`
			);
			return ignoreList.add(txHash);
		}

		const txIn = [{ txHash, txId }];
		const cardanoTip = cardanoCliJs.queryTip();
		const rawTxPath = cardanoCliJs.transactionBuildRaw({
			fee: 0,
			txIn,
			txOut: getTxOut(customerAddress, quantity),
			invalidAfter: cardanoTip.slot + 200,
		});

		const fee = cardanoCliJs.transactionCalculateMinFee({
			txBody: rawTxPath,
			txIn,
			txOut: getTxOut(customerAddress, quantity),
			witnessCount: 2,
		});

		const txPath = cardanoCliJs.transactionBuildRaw({
			fee,
			txIn,
			txOut: getTxOut(customerAddress, quantity, fee),
			invalidAfter: cardanoTip.slot + 200,
		});

		const signedPath = cardanoCliJs.transactionSign({
			txBody: txPath,
			signingKeys: [wallet.paymentSkey.path],
		});

		const refundTransactionId = cardanoCliJs.transactionSubmit(signedPath);

		fs.appendFileSync(
			`./tmp/refunds`,
			`${new Date().toISOString()} - ${refundTransactionId} - ${JSON.stringify(
				getTxOut(customerAddress, quantity, fee)
			)} - Raw: ${rawTxPath} - TxIn: ${JSON.stringify(
				txIn
			)} - Fee: ${fee} - Tx: ${txPath} \n`
		);
	} catch (error) {
		fs.appendFileSync(
			`./tmp/refunds`,
			`${new Date().toISOString()} - ERROR - ${JSON.stringify(error)}\n`
		);
	}
}

module.exports = handleRefund;
