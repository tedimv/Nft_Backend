const CardanocliJs = require('cardanocli-js');
const fs = require('fs');
const { BlockFrostAPI } = require('@blockfrost/blockfrost-js');

const { model } = require('../../db');
const { wallet, blockfrost, cardanoCli, rootDir } = require('../../config');
const { buildRawTx, getNftMeta } = require('./helpers');

const cardanoCliJs = new CardanocliJs({
	socketPath: cardanoCli.socketPath,
	shelleyGenesisPath: cardanoCli.shelleyGenesisPath,
	dir: rootDir,
});

const bfAPI = new BlockFrostAPI({
	isTestnet: false,
	projectId: blockfrost.apiKey,
});

async function handlePurchase(txHash, txIx) {
	let signedPath;
	try {
		const { inputs, outputs } = await bfAPI.txsUtxos(txHash);
		const customerAddress = inputs[0].address;

		const { quantity } = outputs.find(({ address }) => address === wallet.paymentAddr.value)
			.amount[0];

		const unusedNft = await model.findOne({ owner: '' });
		const { meta } = unusedNft;
		const metadata = getNftMeta(meta);

		const txIn = { txHash, txIx };
		const txOut = { customerAddress, quantity };
		const cardanoTip = cardanoCliJs.queryTip();

		const rawTxPath = buildRawTx({
			txIn,
			txOut,
			metadata,
			invalidAfter: cardanoTip.slot + 1000,
		});

		cardanoCliJs.queryProtocolParameters();
		const txFee = cardanoCliJs.transactionCalculateMinFee({
			txBody: rawTxPath,
			txIn: [txIn],
			txOut: [txIn, txIn], // Looks at length
			witnessCount: 2,
		});

		const txPath = buildRawTx({
			txIn,
			txOut,
			metadata,
			invalidAfter: cardanoTip.slot + 2000,
			fee: txFee,
		});

		signedPath = cardanoCliJs.transactionSign({
			txBody: txPath,
			signingKeys: [wallet.paymentSkey.path, wallet.policySkey.path],
		});

		await model.updateOne({ _id: unusedNft._id }, { owner: customerAddress });
		const mintTransactionId = cardanoCliJs.transactionSubmit(signedPath);

		fs.appendFileSync(
			`./tmp/minted`,
			`${new Date().toISOString()} - ${mintTransactionId} - minted ${JSON.stringify({
				id: unusedNft._id,
				...metadata.nftData,
			})}\n`
		);
	} catch (error) {
		console.log({ error });
	}
}

module.exports = handlePurchase;
