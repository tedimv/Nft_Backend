const CardanocliJs = require('cardanocli-js');

const { model } = require('../../db');
const { cardanoCli } = require('../../config');
const handleRefund = require('./refund');
const handlePurchase = require('./purchase');

async function walletScanner() {
	let remaining = await model.find({ owner: '' }).count();

	const cardanoCliJs = new CardanocliJs({
		socketPath: cardanoCli.socketPath,
		shelleyGenesisPath: cardanoCli.shelleyGenesisPath,
	});

	let queueMonitor = [];

	const findTransaction = (txHas) => queueMonitor.find((hash) => hash === txHas);

	const delayedClear = (txHash) => {
		setTimeout(() => {
			console.log(`clearing ${txHash}`);
			queueMonitor = queueMonitor.filter((hash) => hash !== txHash);
		}, 60000);
	};

	let idle = true;

	setInterval(() => {
		// Check for new eUTXOs
		const utxos = cardanoCliJs.queryUtxo(cardanoCli.address).reverse();
		// Add hard stop if everything is sold out
		for (const utxo of utxos) {
			const {
				txHash,
				txId,
				value: { lovelace },
			} = utxo;

			// Prevent paying with shitcoins
			if (isNaN(lovelace)) continue;
			const parsedAmount = cardanoCliJs.toAda(lovelace);
			const hasInvalidAmount = parsedAmount > 30 || parsedAmount <= 29;
			//parsedAmount > 31 || parsedAmount < 29;

			// Wrong amount or sold out
			if (remaining <= 0 || hasInvalidAmount) {
				if (findTransaction(txHash)) continue;
				// TODO: signal remaining 0
				queueMonitor.push(txHash);
				handleRefund(txHash, txId)
					.then(() => delayedClear(txHash))
					.catch(() => null);
				continue;
			}

			if (idle) {
				if (!findTransaction(txHash)) {
					remaining--;
					idle = false;
				} else continue;
				// TODO: signal progress
				queueMonitor.push(txHash);
				handlePurchase(txHash, txId)
					.then(() => {
						delayedClear(txHash);
						idle = true;
					})
					.catch(() => null);
			}
		}
	}, 2000);
}

module.exports = walletScanner;
