const fs = require('fs');
const execSync = require('child_process').execSync;

const { wallet, nft, rootDir } = require('../../config');

function cleanString(string) {
	return string.replace(/\n|\t/gm, '');
}

function getTxOut(customerAddress, quantity, txFee = 0) {
	return [
		{
			address: customerAddress,
			value: {
				lovelace: txFee ? Number(quantity) - txFee : Number(quantity),
			},
		},
	];
}

function getNftMeta(meta) {
	const n = meta.name.match(/[0-9]+/)[0];
	const nftKey = nft.name + n;

	return {
		nftKey,
		nftData: {
			721: {
				[cleanString(wallet.policyID.value)]: {
					[nftKey]: meta,
				},
			},
		},
	};
}

function buildRawTx({ txIn, txOut, fee = 0, metadata, invalidAfter }) {
	const { txHash, txIx } = txIn;
	const { quantity, customerAddress } = txOut;

	const { nftData, nftKey } = metadata;

	const minCustomerQuantity = 1500000;
	const escapeQuantity =
		(fee ? Number(quantity) - Number(fee) : Number(quantity)) - minCustomerQuantity;
	const metadataPath = writeFile({
		type: 'metadata',
		data: nftData,
		fileExt: 'json',
	});

	const outFileId = getUID();
	const outFilePath = `${rootDir}/tmp/tx_${outFileId}.raw`;

	execSync(
		cleanString(`cardano-cli transaction build-raw \                
--fee ${fee} \
--tx-in ${txHash}#${txIx} \                     
--tx-out ${wallet.escapeAddr}+${escapeQuantity} \
--tx-out ${customerAddress}+${minCustomerQuantity}+"1 ${wallet.policyID.value}.${nftKey}" \
--mint="1 ${wallet.policyID.value}.${nftKey}" \
--minting-script-file ${wallet.policyScript.path} \
--metadata-json-file ${metadataPath} \ 
--invalid-hereafter ${invalidAfter} \
--out-file ${outFilePath}`)
	);

	return outFilePath;
}

function writeFile({ type, data, fileExt = '' }) {
	const path = `${rootDir}/tmp/${type}_${getUID()}${fileExt ? `.${fileExt}` : ''}`;
	const content = typeof data === 'object' ? JSON.stringify(data) : data;
	fs.writeFileSync(path, content);
	return path;
}

function getUID() {
	return new Date().getTime().toString();
}

module.exports = {
	buildRawTx,
	getTxOut,
	getNftMeta,
};
