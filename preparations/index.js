const chalk = require('chalk');

const pinataUploadImages = require('./pinata/upload_with_meta');
const pinataDeleteOldImages = require('./pinata/delete_old_imgs');
const dbHydrate = require('./mongodb/hydrate');

(async () => {
	try {
		await pinataDeleteOldImages();
		printDone(1, 2);
	} catch (error) {
		return printError(1);
	}

	try {
		await pinataUploadImages();
		printDone(2, 3);
	} catch (error) {
		return printError(2);
	}

	try {
		await dbHydrate();
		printDone(3);
	} catch (error) {
		return printError(3);
	}
})();

// Helpers
function printDone(prev_num, next_num) {
	let str = `[${prev_num}]`;
	if (next_num) {
		str += ` => [${next_num}] Started`;
	} else {
		str += ' => All steps completed successfully !';
	}

	console.log(chalk.green(str));
}

function printError(step_num) {
	console.log(chalk.red(`[${step_num}] Failed`));
}
