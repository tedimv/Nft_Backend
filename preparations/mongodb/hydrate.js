const fs = require('fs');

const { model } = require('../../db');

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

async function dbHydrate() {
	const objs_folder = `${__dirname}/pre_committed`;
	const files = Array.from(
		new Set(
			fs.readdirSync(objs_folder).map((name) => {
				return name.split('.')[0];
			})
		)
	);

	while (files.length) {
		try {
			const randomIndex = getRandomInt(0, files.length);
			const fileName = files[randomIndex];
			const content = JSON.parse(
				fs.readFileSync(`${objs_folder}/${fileName}.json`, {
					encoding: 'ascii',
				})
			);
			await model.create(content);
			files.splice(randomIndex, 1);
		} catch (error) {
			throw error;
		}
	}
}

module.exports = dbHydrate;
