const axios = require('axios');

const { pinata } = require('../../config');

async function pinataDeleteOldImages() {
	const url = `${pinata.baseUrl}/psa/pins`;
	const config = {
		headers: {
			Authorization: `Bearer ${pinata.jwt}`,
		},
	};

	const response = await axios.get(url, config);

	for await (const img of response.data.results) {
		await axios.delete(`${url}/${img.requestid}`, config);
	}
}

module.exports = pinataDeleteOldImages;
