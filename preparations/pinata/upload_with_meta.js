const fs = require('fs');
const FormData = require('form-data');
const axios = require('axios');

const { pinata } = require('../../config');

// Place images and json meta data in the [imgs] folder
const imgsPath = __dirname.replace('pinata', 'imgs');

async function uploadFile(nft_num, meta_parsed_rich) {
	try {
		const fd = new FormData();
		fd.append('file', fs.createReadStream(`${imgsPath}/${nft_num}.png`));
		fd.append('pinataMetadata', JSON.stringify(meta_parsed_rich));

		const res = await axios.post(
			`${pinata.baseUrl}/pinning/pinFileToIPFS`,
			fd,
			{
				maxBodyLength: 'infinity',
				headers: {
					Authorization: `Bearer ${pinata.jwt}`,
					'Content-Type': `multipart/form-data; boundary=${fd.getBoundary()}`,
				},
			}
		);

		return `ipfs://${res.data.IpfsHash}`;
	} catch (error) {}
}

async function pinataUploadImages() {
	const all_files = fs.readdirSync(imgsPath);
	const names = new Set(all_files.map((name) => name.split('.')[0]));

	// Generate range of [nft numbers] and [ids]
	const remainingIds = new Array(names.size).fill().map((_, i) => i + 1);

	for await (const nft_num of names) {
		let meta_parsed = JSON.parse(
			fs.readFileSync(`${imgsPath}/${nft_num}.json`, {
				encoding: 'ascii',
			})
		);

		const randomIndex = getRandomInt(0, remainingIds.length);
		const selectedId = remainingIds[randomIndex];
		const name = `MyTestAndFakeNft #${selectedId}`;
		remainingIds.splice(randomIndex, 1);

		const obj = {
			_id: selectedId,
			owner: '',
			meta: {
				name,
				...meta_parsed,
				image: await uploadFile(nft_num, {
					name,
					keyvalues: meta_parsed,
				}),
			},
		};

		const result_file_path =
			__dirname.replace('pinata', 'mongodb/pre_committed') + `/${nft_num}.json`;
		fs.writeFileSync(result_file_path, JSON.stringify(obj));
	}
}

// Helpers
function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

module.exports = pinataUploadImages;
